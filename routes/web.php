<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $category = App\Category::all();
    $news = App\News::all();
    $category = App\Category::all();
    $categories = App\Category::with(['news' => function($query){
        $query->orderBy('updated_at', 'DESC')->get();
    }])->get();
    $news_today = App\News::whereDate('created_at', Carbon\Carbon::today())->get();
    return view('home', compact('category', 'categories', 'news', 'news_today'));
});



// Route::post('/reg', 'HomeController@post');
// Route::get('/reg', function(){
//     return view ('welcome');
// });
Auth::routes();
Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');

    //show profile
    Route::get('/profile', 'ProfileController@index');
    Route::put('/profile/{user_id}', 'ProfileController@update');

    //show news
    Route::get('/news/create', 'NewsController@create');
    Route::post('/news', 'NewsController@store');
    Route::get('/news/{news_id}/edit', 'NewsController@edit');
    Route::put('/news/{news_id}', 'NewsController@update');
    Route::delete('/news/{news_id}', 'NewsController@destroy');

    //show comment
    Route::put('/comment/{comment_id}', 'CommentController@store');
    Route::delete('/comment/{comment_id}', 'CommentController@destroy');
});
// untuk user yg tidak login, biar tetap tampil beritannya
Route::get('/news/{news_id}', 'NewsController@show');

