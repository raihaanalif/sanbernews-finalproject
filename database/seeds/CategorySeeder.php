<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::insert([
            [
                'nama' => 'Olahraga'
            ],
            [
                'nama' => 'Entertaiment'
            ],
            [
                'nama' => 'Politik'
            ],
            [
                'nama' => 'Kesehatan'
            ]
        ]);
    }
}
