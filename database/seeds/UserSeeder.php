<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
            [
            'first_name' => 'admin',
            'last_name' => 'aja',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'role' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ],
        [
            'first_name' => 'admin',
            'last_name' => 'doang',
            'email' => 'admin1@gmail.com',
            'password' => bcrypt('password1'),
            'role' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]
        ]);
    }
}
