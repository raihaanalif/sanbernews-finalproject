<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\News;
use App\User;
use Alert;
use Illuminate\Support\Facades\Storage;


class NewsController extends Controller
{

    public function create(){
        $id = Auth::user()->id;
        $user = User::find($id);
        $category = Category::all();
        return view('pages.admin.create', compact('user', 'category'));
    }

    public function store(Request $request){
        // dd($request->all());
        // dd (Auth::user()->id);
        // dd($request->file('gambar')->extension());

        $file = $request->file('gambar');
        if($file !== null){
            $extension = $file->extension();
            $img = date('dmyHis').'.'.$extension;
        }
        $request->validate([
            'judul' => 'required',
            'category_id' => 'required',
            'isi' => 'required',
            'gambar' => 'required|file|max:2048',
        ]);
        $path = Storage::putFileAs('public/image', $file, $img);

        $user_id = Auth::user()->id;
        News::create([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'gambar' => $img,
            'category_id' => $request->category_id,
            'user_id' => $user_id,
        ]);
        
        Alert::success('Done', 'Berita di Tambahkan');
        return redirect('/home')->with($path);
    }

    public function show($id){
        $news = News::find($id);
        $category = Category::all();
        if(Auth::guest()){
            $user_id = $news->user_id;
            $author = User::find($user_id);
            $comment = Comment::all()->where('news_id', $id);

            return view ('pages.user.show', compact('news', 'category', 'comment', 'author'));
        }else{
            $user_id = $news->user_id;
            $author = User::find($user_id);
            $comment = Comment::all()->where('news_id', $id);
            $sid = Auth::user()->id;
            $user = User::find($sid);
            $role = Auth::user()->role;
            $user_id = $news->user_id;
            $author = User::find($user_id);
            $comment = Comment::all()->where('news_id', $id);

            if($role == 1){
                return view ('pages.user.show', compact('news', 'category', 'user', 'comment', 'author'));
            }else{
                return view ('pages.admin.show', compact('news', 'category', 'user', 'comment', 'author'));
            }
        }
    }

    public function edit($id){
        // dd(News::find($id)->user_id);
        $news = News::find($id);
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        $category = Category::all();
        if($user_id == $news->user_id){
            return view('pages.admin.edit', compact('news', 'category', 'user'));
        }
        else{
            return redirect()->back();
        }
    }

    public function update($id, Request $request){
        $file = $request->file('gambar');
        if($file !== null){
            $extension = $file->extension();
            $img = date('dmyHis').'.'.$extension;
        }
        $request->validate([
            'judul' => 'required',
            'category_id' => 'required',
            'isi' => 'required',
            'gambar' => 'required|file|max:2048',
        ]);
        $path = Storage::putFileAs('public/image', $file, $img);
        $news = News::find($id);
        $news->judul = $request->judul;
        $news->isi = $request->isi;
        $news->category_id = $request->category_id;
        $news->gambar = $img;
        $news->update();
        return redirect('/home')->with($path);
    }

    public function destroy($id){
        $user = Auth::user()->id;
        $news = News::find($id);
        if($user == $news->user_id){
            $news->delete();
            return redirect ('/home');
        }
        else{
            return redirect('/home');
        }
    }
}
