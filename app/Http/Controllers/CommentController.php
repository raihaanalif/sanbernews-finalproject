<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;


class CommentController extends Controller
{
    public function store($id, Request $request){
        $request->validate([
            'isi'
        ]);

        $user_id = Auth::user()->id;

        Comment::create([
            'isi' => $request->isi,
            'user_id' => $user_id,
            'news_id' => $id,
        ]);

        return redirect()->back();
    }

    public function destroy($id){
        $user_id = Auth::user()->id;
        $comment = Comment::find($id);
        if($user_id == $comment->user_id){
            $comment->delete();
            return redirect()->back();
        }
        else{
            return redirect()->back();
        }
    }
}
