<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Category;
use App\News;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(Category::with(['news' => function($query){
        //     $query->orderBy('updated_at', 'DESC')->get();
        // }])->get());
        // dd(News::all()->where('category_id', 1));
        // dd((Category::all()->pluck('id') == News::all()->pluck('category_id')));
        $role = Auth::user()->role;
        $id = Auth::user()->id;
        $user = User::find($id);
        $news = News::all();
        $news_today = News::whereDate('created_at', Carbon::today())->get();
        $category = Category::all();
        $categories = Category::with(['news' => function($query){
            $query->orderBy('updated_at', 'DESC')->get();
        }])->get();

        if($role == 1){
            return view('pages.user.index', compact('user', 'news', 'category', 'categories', 'news_today'));
        }
        else{
            return view('pages.admin.index', compact('user', 'news', 'category', 'categories', 'news_today'));
        }
    }

    // public function post(Request $request){
    //     dd($request->all());
    // }
}
