<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $id = Auth::user()->id;
        $user = User::find($id);
        $category = Category::all();
        return view('pages.profile', compact('user', 'category'));
    }

    public function update($id, Request $request){
        // dd(Profile::select('umur')->where('user_id', $id)->first());
        $request->validate([
            'first_name',
            'last_name',
            'email',
            'umur' => 'nullable',
            'alamat' => 'nullable',
            'bio' => 'nullable',
        ]);
        $user = User::find($id);
        if(empty(Profile::select('umur')->where('user_id', $id)->first())){
            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
            ]);
            $profile = new Profile;
            $profile->umur = $request->umur;
            $profile->alamat = $request->alamat;
            $profile->bio = $request->bio;
            $profile->user_id = $id;
            $profile->save();
        }else{
            $profile = $user->profile;
            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
            ]);
            $profile->update([
                'umur' => $request->umur,
                'alamat' => $request->alamat,
                'bio' => $request->bio,
            ]);
        }
        return redirect('/profile');
    }
}
