<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";

    public $timestamps = true;

    protected $fillable = [
        'judul', 'isi', 'gambar', 'category_id', 'user_id',
    ];
    public function category(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comment(){
        return $this->hasMany('App\Comment');
    }
}
