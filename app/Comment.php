<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    public $timestamps = true;

    protected $fillable = [
        'isi', 'user_id', 'news_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function news(){
        return $this->belongsTo('App\News');
    }

}
