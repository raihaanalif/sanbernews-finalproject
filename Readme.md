# Final Project
## Kelompok 17
## Anggota Kelompok
- Raihan Alifianto (@jusscallmee)
- Ricky Supriyanto (@ricky_s20)
- Thomas Dwi Awaka (@misdinar)

## Tema Project
Website CRUD Berita
## ERD
<img src="erd.PNG" alt="ERD Berita">

## Link Video
Link Demo Aplikasi : https://youtu.be/UH-pGoXzwRQ

Link Deploy : http://sanbernews.herokuapp.com/
