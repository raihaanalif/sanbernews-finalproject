@extends('layouts.app')

@section('content')
<div class="section-body">
    <h2 class="section-title">Forms</h2>
    <form action="{{'/news'}}" method="POST" enctype="multipart/form-data">
        <div class="card">
          <div class="card-body">
            @csrf
            <div class="form-group"
              <label for="judul">Judul Berita</label>
              <input id="judul" type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" value="{{ old('judul') }}" required>
              @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
              <label for="category_id">Kategori Berita</label>
              <select id="category_id" class="form-control  @error('category_id') is-invalid @enderror" name="category_id" required>
                @foreach ($category as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
              </select>
              @error('category_id')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="isi">Isi Berita</label>
              <textarea id="isi" class="form-control @error('isi') is-invalid @enderror" name="isi" required>{{old('isi')}}</textarea>
              @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="gambar">Gambar Terkait</label>
              <input id="gambar" type="file" class="form-control @error('gambar') is-invalid @enderror" name="gambar" required>
              @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <div class="card-footer text-right">
            <button class="btn btn-primary mr-1" type="submit">Submit</button>
          </div>
        </div>
      </form>
  </div>

@endsection
