@extends('layouts.app')

@section('content')
<div class="section-body">
    <h2 class="section-title">Hi, {{$user->first_name}} {{$user->last_name}}!</h2>
    <p class="section-lead">
      Change information about yourself on this page.
    </p>

    <div class="row mt-sm-4">
      <div class="col-12 col-md-12 col-lg-5">
        <div class="card profile-widget">
          <div class="profile-widget-header">
            <img alt="image" src="../assets/img/avatar/avatar-1.png" class="rounded-circle profile-widget-picture">
          </div>
          <div class="profile-widget-description">
            <div class="profile-widget-name">{{$user->first_name}} {{$user->last_name}} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div>@if(isset($user->profile->umur)){{$user->profile->umur}} @endif/ {{$user->email}} <br> @if(isset($user->profile->alamat)){{$user->profile->alamat}}@endif</div></div>
            @if(isset($user->profile->bio)){{$user->profile->bio}} @endif
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-7">
        <div class="card">
          <form method="POST" class="needs-validation" action="/profile/{{$user->id}}">
            @csrf
            @method('PUT')
            <div class="card-header">
              <h4>Edit Profile</h4>
            </div>
            <div class="card-body">
                <div class="row">
                  <div class="form-group col-md-6 col-12">
                    <label>First Name</label>
                    <input name="first_name" type="text" class="form-control" value="{{$user->first_name}}"required="">
                    <div class="invalid-feedback">
                      Please fill in the first name
                    </div>
                  </div>
                  <div class="form-group col-md-6 col-12">
                    <label>Last Name</label>
                    <input name="last_name" type="text" class="form-control" value="{{$user->last_name}}" required="">
                    <div class="invalid-feedback">
                      Please fill in the last name
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-7 col-12">
                    <label>Email</label>
                    <input name="email" type="email" class="form-control" value="{{$user->email}}" required="">
                    <div class="invalid-feedback">
                      Please fill in the email
                    </div>
                  </div>
                  <div class="form-group col-md-5 col-12">
                    <label>Umur</label>
                    <input name="umur" type="number" class="form-control" value="@if(isset($user->profile->umur)){{$user->profile->umur}}@endif">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-12">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control summernote-simple" autocomplete="alamat">@if(isset($user->profile->alamat)){{$user->profile->alamat}}@endif</textarea>
                  </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                      <label>Bio</label>
                      <textarea name="bio" class="form-control summernote-simple">@if(isset($user->profile->bio)){{$user->profile->bio}}@endif</textarea>
                    </div>
                  </div>
                <div class="row">
                  <div class="form-group mb-0 col-12">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" id="newsletter">
                      <label class="custom-control-label" for="newsletter">Subscribe to newsletter</label>
                      <div class="text-muted form-text">
                        You will get new information about products, offers and promotions
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="card-footer text-right">
              <button type="submit" class="btn btn-primary">Save Changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
