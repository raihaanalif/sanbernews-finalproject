@extends('layouts.app')
@section('content')
<h1 class="text-center">{{$news->judul}}</h1>
<p class="text-center">{{$news->created_at->format('Y-m-d')}}</p>
<hr style="height:2px;border-width:0;color:gray;background-color:gray">

    <div class="card">
        <div class="card-header">
            <img class="d-block w-100" src="../storage/image/{{ $news->gambar }}" alt="" style="height: 500px !important;">
        </div>
        <div class="card-body">
            <p class="lead text-justify">{{$news->isi}}</p>
        </div>
        <div class="card-footer bg-whitesmoke">
            <p class="lead"> Author : <a style="cursor: pointer;" onMouseOver="this.style.color='#00F'" data-toggle="modal" data-target="#exampleModal"><b>{{$author->first_name}} {{$author->last_name}}</b></a></p>
        </div>
        <div class="card">
            <h3 class="text-center">Comment</h3>
            <hr style="height:2px;border-width:0;color:gray;background-color:gray; width:60%; margin: 0 auto;">
            <br>
            <form action="/comment/{{$news->id}}" method="POST" style="margin: 0 auto; width: 60%">
                @csrf
                @method('PUT')
                <textarea class="form-control" placeholder="Add your comment" name="isi"></textarea>
                <button class="btn btn-primary" type="submit" style="margin-top: 10px">Submit</button>
            </form>
            <div class="card-body" style="margin: 0 auto; width: 90%">
                <div class="table-responsive">
                @foreach ($comment as $item)
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td class="col"><b>{{$item->user->first_name}} {{$item->user->last_name}}</b><br>{{$item->isi}}</td>
                            <td class="col-md-auto">{{$item->created_at->format('Y-m-d')}}</td>
                            <td class="col col-lg-2">
                                <form action="/comment/{{$item->id}}" method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
                @endforeach
                </div>
              </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Profil Author</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="card profile-widget">
                        <div class="profile-widget-header">
                          <img alt="image" src="../assets/img/avatar/avatar-1.png" class="rounded-circle profile-widget-picture">
                        </div>
                        <div class="profile-widget-description">
                          <div class="profile-widget-name">{{$author->first_name}} {{$author->last_name}} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div>{{$author->profile->umur}} / {{$author->email}} <br> {{$author->profile->alamat}}</div></div>
                          {{$author->profile->bio}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
