@extends('layouts.app')

@section('content')

<div class="card-body">
    <section id="trend">
        <h1 class="section-title">Headline</h1>
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
            </ol>
            <div class="carousel-inner">
                @foreach ($news as $key => $berita)
                <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                <a href="/news/{{$berita->id}}" class="dropdown-item has-icon">
                <img class="d-block w-100" src="../storage/image/{{ $berita->gambar }}" alt="" style=" width:100%; height: 700px !important;">
                  <div class="carousel-caption d-none d-md-block" style="background-color: black; opacity: 0.5;">
                    <h5><b>{{$berita->judul}}</b></h5>
                    <p class="lead">{!! Illuminate\Support\Str::words($berita->isi, 8, '....')!!}</p>
                  </div>
                </a>
                </div>
                @endforeach

            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
    </section>
    <br>
    <section>
        @foreach ($categories as $item)
        <h1 class="section-title" id="{{$item->nama}}">{{$item->nama}}</h1>
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
            <div class="row">
                @foreach ($item->news as $items)
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <article class="article article-style-b">
                      <div class="article-header">
                        <div class="article-image" data-background="../storage/image/{{$items->gambar}}">
                        </div>
                      </div>
                      <div class="article-details">
                        <div class="article-title">
                          <h2><a href="/news/{{$items->id}}">{{$items->judul}}</a></h2>
                        </div>
                        <p>{!! Illuminate\Support\Str::words($items->isi, 6, '....')!!}</p>
                        <div class="article-cta">
                          <a href="/news/{{$items->id}}">Read More <i class="fas fa-chevron-right"></i></a>
                        </div>
                      </div>
                    </article>
                  </div>
                @endforeach
            </div>
        @endforeach
    </section>
    <section id="today">
        <h1 class="section-title">Today-News</h1>
        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
        <div class="row">
            @foreach ($news_today as $items)
        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
            <article class="article article-style-b">
              <div class="article-header">
                <div class="article-image" data-background="../storage/image/{{$items->gambar}}">
                </div>
              </div>
              <div class="article-details">
                <div class="article-title">
                  <h2><a href="/news/{{$items->id}}">{{$items->judul}}</a></h2>
                </div>
                <p>{!! Illuminate\Support\Str::words($items->isi, 6, '....')!!}</p>
                <div class="article-cta">
                  <a href="/news/{{$items->id}}">Read More <i class="fas fa-chevron-right"></i></a>
                </div>
              </div>
            </article>
          </div>
        @endforeach
        </div>
    </section>
  </div>
  @endsection
