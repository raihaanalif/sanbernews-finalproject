<nav class="navbar navbar-expand-lg main-navbar fixed-top">
    @guest
    <a href="/" class="navbar-brand sidebar-gone-hide">SanberNews</a>
    @else
    <a href="/home" class="navbar-brand sidebar-gone-hide">SanberNews</a>
    @endguest
    <div class="navbar-nav">
      <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
    </div>
    <div class="nav-collapse">
      <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
        <i class="fas fa-ellipsis-v"></i>
      </a>
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#trend" class="nav-link">Headline</a></li>
        <li class="nav-item dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">Category</a>
            <div class="dropdown-menu">
                @foreach ($category as $nav)
                <a href="#{{$nav->nama}}" class="dropdown-item">{{$nav->nama}}</a>
                @endforeach
            </div>
        </li>
        <li class="nav-item"><a href="#today" class="nav-link">Today News</a></li>
      </ul>
    </div>
    <form class="form-inline ml-auto">
    </form>
    <ul class="navbar-nav navbar-right">
      <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        @guest
            <div class="d-sm-none d-lg-inline-block">Please Login First</div></a>
        @else
            <div class="d-sm-none d-lg-inline-block">Hi, {{$user->first_name}} {{$user->last_name}}</div></a>
        @endguest

        <div class="dropdown-menu dropdown-menu-right">
            @guest
                <a class="dropdown-item has-icon" href="{{ route('login') }}">
                    {{ __('Login') }}
                </a>
                @if (Route::has('register'))
                    <a class="dropdown-item has-icon" href="{{ route('register') }}">
                        {{ __('Register') }}
                    </a>
                @endif
            @else
                <a href="{{'/profile'}}" class="dropdown-item has-icon">
                    <i class="far fa-user"></i> Profile
                </a>
                @if((Auth::user()->role)== 2)
                    <a href="{{'/news/create'}}" class="dropdown-item has-icon">
                        <i class="fa fa-newspaper"></i> Create News
                    </a>
                @endif
                <div class="dropdown-divider"></div>
                <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger"
                    onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                </form>

            @endguest
        </div>
      </li>
    </ul>
  </nav>
